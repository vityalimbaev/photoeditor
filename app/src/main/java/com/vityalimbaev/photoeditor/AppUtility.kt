package com.vityalimbaev.photoeditor

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import java.io.File


lateinit var uri:Uri

fun loadBitmap(context: Context, photoUri: Uri): Bitmap? {
    uri = photoUri
    val imageStream = context.contentResolver.openInputStream(photoUri)
    return BitmapFactory.decodeStream(imageStream)
}

fun loadScaledBitmap(context: Context, photoUri: Uri, width:Int): Bitmap {
    val bitmap = loadBitmap(context,photoUri)
    val heightBitmap = bitmap?.height
    val widthBitmap = bitmap?.width
    val scale = width.toFloat() / widthBitmap!!
    val scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, (heightBitmap!!.toFloat() * scale).toInt(),true)
    return scaledBitmap
}

fun getDisplayWidth(activity: AppCompatActivity): Int {
    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun galleryAddPic(context: Context, uri:Uri) {
    val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
    mediaScanIntent.data = uri
    context.sendBroadcast(mediaScanIntent)
}