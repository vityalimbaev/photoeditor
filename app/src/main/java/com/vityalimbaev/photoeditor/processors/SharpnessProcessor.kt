package com.vityalimbaev.photoeditor.processors

import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.RenderScript
import android.renderscript.Type
import com.vityalimbaev.photoeditor.START_CENTRE
import com.vityalimbaev.photoeditor.START_LEFT
import com.vityalimbaev.photoeditor.ScriptC_ImgProcessor

class SharpnessProcessor:ProcessorInterface {
    override lateinit var bitmap: Bitmap
    override var progress = 0
    override val controlMode = START_CENTRE

    override fun setInputAllocation(bitmap: Bitmap) {
        this.bitmap = bitmap.copy(bitmap.config,true)
    }

    override fun execute(rs:RenderScript, scriptC: ScriptC_ImgProcessor, outputType: Type, value:Int):Bitmap {
        progress = value
        val outAllocation = Allocation.createTyped(rs, outputType)
        val inAllocation = Allocation.createFromBitmap(rs, bitmap)
        scriptC.invoke_sharpness((value)/100.toFloat(), inAllocation, outAllocation)
        val tempBitmap = Bitmap.createBitmap(bitmap)
        outAllocation.copyTo(tempBitmap)
        return tempBitmap
    }

    override fun getPreviousBitmap(bitmap:Bitmap): Bitmap {
        return bitmap
    }
}