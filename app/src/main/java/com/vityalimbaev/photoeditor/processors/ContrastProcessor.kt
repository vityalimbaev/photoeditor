package com.vityalimbaev.photoeditor.processors

import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.RenderScript
import android.renderscript.Type
import com.vityalimbaev.photoeditor.START_CENTRE
import com.vityalimbaev.photoeditor.ScriptC_ImgProcessor

class ContrastProcessor:ProcessorInterface {
    override lateinit var bitmap: Bitmap
    override var progress = 0
    override val controlMode = START_CENTRE

    override fun setInputAllocation(bitmap: Bitmap) {
        this.bitmap = bitmap.copy(bitmap.config,true)
    }

    override fun execute(rs:RenderScript, scriptC: ScriptC_ImgProcessor, outputType: Type, value:Int):Bitmap {
        progress = value
        val outAllocation = Allocation.createTyped(rs, outputType)
        val inAllocation = Allocation.createFromBitmap(rs, bitmap)
        var v = value/50.0f
        v = if(v < 0.2f) 0.2f else v
        v = if(v > 1.8f) 1.8f else v
        scriptC.invoke_contrast(v, inAllocation, outAllocation)
        val tempBitmap = Bitmap.createBitmap(bitmap)
        outAllocation.copyTo(tempBitmap)
        return tempBitmap
    }

    override fun getPreviousBitmap(bitmap:Bitmap): Bitmap {
        return bitmap
    }
}