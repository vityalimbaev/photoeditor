package com.vityalimbaev.photoeditor.processors

import android.graphics.Bitmap
import android.renderscript.*
import android.util.Log
import com.vityalimbaev.photoeditor.START_CENTRE
import com.vityalimbaev.photoeditor.START_LEFT
import com.vityalimbaev.photoeditor.ScriptC_ImgProcessor

val EMPTY_MODE = -1
val SATURATION_MODE = 0
val CONTRAST_MODE = 1
val BRIGHTNESS_MODE = 2
val EXPOSURE_MODE = 3
val BLUR_MODE = 4
val SHARPNESS_MODE = 5

class ProcessorsManger(var bitmap: Bitmap, private val rs: RenderScript, private val scriptC: ScriptC_ImgProcessor) {

    private val activeProcessors = mutableMapOf<Int, ProcessorInterface>()
    var curBitmap: Bitmap = Bitmap.createBitmap(bitmap)
    private val queue = ArrayList<Int>()
    private val outputType:Type = Type.Builder(rs, Element.RGBA_8888(rs))
        .setX(bitmap.width)
        .setY(bitmap.height)
        .create()

    fun executeProcess(mode: Int, value: Int): Bitmap {
        if (mode != EMPTY_MODE) {
            if(queue.indexOf(mode) < queue.size-1 && queue.contains(mode)){
                val index = queue.indexOf(mode)
                queue.remove(mode)
                val processor = getProcessor(mode)
                processor.setInputAllocation(serialExecute(processor.bitmap, index))
            }
                curBitmap = getProcessor(mode).execute(rs, scriptC, outputType, value)

        }
        return curBitmap
    }

    fun serialExecute(startBitmap:Bitmap, startIndexMode:Int):Bitmap{
        var tempbitmap = startBitmap
        val outputTypeForCurBitmap = if(bitmap.height != startBitmap.height){
            Type.Builder(rs, Element.RGBA_8888(rs))
                .setX(startBitmap.width)
                .setY(startBitmap.height)
                .create()

        }else{
            outputType
        }
        Log.d("RENDERMAN", "serialExecute: "+"BITMAP_EXIST: ${startBitmap.height}")
        for(i:Int in startIndexMode until queue.size){
            val processor = getProcessor(queue[i])
            processor.setInputAllocation(tempbitmap)
            tempbitmap = processor.execute(rs,scriptC,outputTypeForCurBitmap,processor.progress)
        }
        return tempbitmap
    }

    fun getProgress(mode: Int): Int {
        var progress = -1
        if (activeProcessors.containsKey(mode)) {
            progress = activeProcessors[mode]!!.progress
        }
        return progress
    }

    fun saveResult(mode: Int) {
        val processor = getProcessor(mode)
        if (processor.controlMode == START_CENTRE && processor.progress != 50 ||
            processor.controlMode == START_LEFT && processor.progress != 0
        ) {
            queue.add(mode)
        } else {
            removeResult(mode)
        }
    }

    fun removeResult(mode: Int): Bitmap {
        val processor = getProcessor(mode)
        curBitmap = processor.bitmap.copy(processor.bitmap.config, true)
        activeProcessors.remove(mode)
        queue.remove(mode)
        return curBitmap
    }

    private fun getProcessor(mode: Int): ProcessorInterface {
        val processor: ProcessorInterface
        if (!activeProcessors.containsKey(mode)) {
            processor = initProcessor(mode)!!
            processor.setInputAllocation(curBitmap)
            activeProcessors[mode] = processor
        } else {
            processor = activeProcessors[mode]!!
        }
        return processor
    }

    private fun initProcessor(mode: Int): ProcessorInterface? {
        when (mode) {
            SATURATION_MODE -> return SaturationProcessor()
            CONTRAST_MODE -> return ContrastProcessor()
            BRIGHTNESS_MODE -> return BrightnessProcessor()
            EXPOSURE_MODE -> return ExposureProcessor()
            BLUR_MODE -> return BlurProcessor()
            SHARPNESS_MODE -> return SharpnessProcessor()
        }
        return null
    }

    fun getOriginalBitmap():Bitmap{
        return bitmap
    }
}