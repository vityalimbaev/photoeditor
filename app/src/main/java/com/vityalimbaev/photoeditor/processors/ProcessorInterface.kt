package com.vityalimbaev.photoeditor.processors

import android.graphics.Bitmap
import android.renderscript.*
import com.vityalimbaev.photoeditor.ScriptC_ImgProcessor

interface ProcessorInterface {
    val progress: Int
    val controlMode: Int
    val bitmap: Bitmap

    fun setInputAllocation(bitmap: Bitmap)
    fun execute(
        rs: RenderScript,
        scriptC: ScriptC_ImgProcessor,
        outputType: Type,
        value: Int
    ): Bitmap
    fun getPreviousBitmap(bitmap: Bitmap): Bitmap
}