package com.vityalimbaev.photoeditor

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatSeekBar
import kotlin.math.abs

val START_LEFT = 0
val START_CENTRE = 1

class CustomSeekBar :AppCompatSeekBar {

    var colorProgress = Color.MAGENTA
    private var paint: Paint? = null
    private var seekbar_height = 0
    var drawMode = START_LEFT

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        paint = Paint()
        seekbar_height = 10
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
    }

    private fun getRectFromCentre(): Rect? {
        val length = max + abs(min)
        val rect:Rect = Rect()
        if (progress > 50) {
            rect.set(
                width / 2,
                height / 2 - seekbar_height / 2,
                width / 2 + width / (length) * (progress - (length/2)),
                height / 2 + seekbar_height / 2
            )
        }
        if (progress < 50) {
            rect.set(
                width / 2 - width / (length) * ((length)/2 - abs(progress)),
                height / 2 - seekbar_height / 2,
                width / 2,
                height / 2 + seekbar_height / 2
            )

        }
        return rect
    }

    private fun getRectFromLeft(): Rect? {
        val length = max + abs(min)
        val rect = Rect()
        var right = (width/length)*(abs(min)+abs(progress))
        var right2 = width - (width/length)*abs(max - progress)
        if(right != right2){
            right += (right2 - right) / 2
        }
        rect.set(
            thumbOffset,
            height / 2 - seekbar_height / 2,
            right,
            height / 2 + seekbar_height / 2
        )
        return rect
    }

    override fun onDraw(canvas: Canvas) {
        val rect = Rect()
        rect.set(0 + thumbOffset,
                height / 2 - seekbar_height / 2,
                width - thumbOffset,
                height / 2 + seekbar_height / 2)
        paint!!.color = Color.GRAY
        canvas.drawRect(rect, paint!!)
        if(drawMode == START_CENTRE) {
            paint!!.color = colorProgress
            canvas.drawRect(getRectFromCentre()!!, paint!!)
        }
        if (drawMode == START_LEFT){
            paint!!.color = colorProgress
            canvas.drawRect(getRectFromLeft()!!, paint!!)
        }
        super.onDraw(canvas)
    }
}