package com.vityalimbaev.photoeditor.activities

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.renderscript.RenderScript
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vityalimbaev.photoeditor.*
import com.vityalimbaev.photoeditor.processors.*
import kotlinx.android.synthetic.main.activity_editor.*
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class EditorActivity : AppCompatActivity(), OnSeekBarChangeListener {
    private val WRITE_DATA_PRIMISSION_CODE = 129
    private var currentMode = EMPTY_MODE

    private lateinit var imageView: ImageView
    private lateinit var textView: TextView
    private lateinit var seekBar: CustomSeekBar
    private lateinit var rootcontainer: ConstraintLayout
    private lateinit var scrollView: HorizontalScrollView
    private lateinit var seekBarContainer: ConstraintLayout

    private lateinit var photoUri: Uri
    private lateinit var bitmap: Bitmap

    private lateinit var processorManager:ProcessorsManger
    private var originalFlag = false
    private var currentProgress = 0
    private var accessCallRenderScript = false
    private val anim = Animations(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)

        seekBarContainer = findViewById(R.id.seekbarContainer)
        scrollView = findViewById(R.id.scrollView)
        rootcontainer = findViewById(R.id.rootcontainer)
        textView = findViewById(R.id.textView2)
        seekBar = findViewById(R.id.customSeekBar)
        seekBar.colorProgress = ContextCompat.getColor(this,
            R.color.colorSeekBarProgress
        );

        prepareBitmap(intent.extras?.get("photoUri") as Uri)
        intent.extras?.get("photoUri")?.let {  prepareBitmap(it as Uri)}

        imageView = findViewById(R.id.imageView)
        imageView.setImageBitmap(bitmap)

        val rs = RenderScript.create(this)
        val scriptC = ScriptC_ImgProcessor(rs)
        scriptC._width = bitmap.width.toFloat()
        scriptC._hight = bitmap.height.toFloat()
        processorManager = ProcessorsManger(bitmap, rs, scriptC)

        seekBar = findViewById(R.id.customSeekBar)
        seekBar.setOnSeekBarChangeListener(this)
        textView.setText(seekBar.progress.toString())
    }

    private fun prepareBitmap(uri:Uri) {
        photoUri = uri
        val width = getDisplayWidth(this)
        bitmap =
            loadScaledBitmap(this, photoUri, width)
    }


    fun saturation(v: View) {
        prepareControls(SATURATION_MODE)
    }

    fun contrast(v: View) {
        prepareControls(CONTRAST_MODE)
    }

    fun brightness(v: View) {
        prepareControls(BRIGHTNESS_MODE)
    }

    fun blur(v: View) {
        prepareControls(BLUR_MODE)
    }

    fun exposure(v: View) {
        prepareControls(EXPOSURE_MODE)
    }

    fun sharpness(v: View) {
        prepareControls(SHARPNESS_MODE)
    }

    fun showOriginalBitmap(v:View){
        val btn = v as Button
        if(originalFlag) {
            imageView.setImageBitmap(processorManager.curBitmap)
            btn.setBackgroundResource(R.drawable.shape_btn_eage)
            btn.setTextColor(ContextCompat.getColor(this,
                R.color.colorSoftWhite
            ))
        }else{
            imageView.setImageBitmap( processorManager.getOriginalBitmap())
            btn.setBackgroundResource(R.drawable.shape_btn_fill)
            btn.setTextColor(Color.BLACK)
        }
        originalFlag = !originalFlag
    }

    fun prepareControls(mode: Int) {
        currentMode = mode
        val progress = processorManager.getProgress(currentMode)
        when (mode) {
            SATURATION_MODE -> {
                seekBar.drawMode = START_CENTRE
                seekBar.progress = if(progress == -1) 50 else progress
            }
            CONTRAST_MODE -> {
                seekBar.drawMode = START_CENTRE
                seekBar.progress = if(progress == -1) 50 else progress
            }
            BRIGHTNESS_MODE -> {
                seekBar.drawMode = START_CENTRE
                seekBar.progress = if(progress == -1) 50 else progress
            }
            EXPOSURE_MODE -> {
                seekBar.drawMode = START_CENTRE
                seekBar.progress = if(progress == -1) 50 else progress
            }
            BLUR_MODE -> {
                seekBar.drawMode = START_LEFT
                seekBar.progress = if(progress == -1) 0 else progress
            }
            SHARPNESS_MODE -> {
                seekBar.drawMode = START_LEFT
                seekBar.progress = if(progress == -1) 0 else progress
            }
        }
        seekBar.invalidate()
        startAnim()
    }

    override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
        if(accessCallRenderScript) {

            var progress = 0
            progress = if (customSeekBar.drawMode == START_CENTRE) {
                (i - 50)
            } else {
                i
            }
            currentProgress = progress
            textView.text = progress.toString()
            imageView.setImageBitmap(processorManager.executeProcess(currentMode, i))
        }
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
        accessCallRenderScript = true
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
        accessCallRenderScript = false
    }

    fun back(v: View) {
        imageView.setImageBitmap(processorManager.removeResult(currentMode))
        closeMode()
    }

    fun save(v: View) {
        processorManager.saveResult(currentMode)
        closeMode()
    }

    fun closeMode() {
        changeColorBtn()
        startAnim()
        currentMode = EMPTY_MODE
        currentProgress = 0
    }

    fun startAnim() {
        anim.open(seekBarContainer, scrollView)
    }

    fun changeColorBtn(){
        val visibility = if(currentProgress == 0) View.INVISIBLE else View.VISIBLE

        when(currentMode){
            CONTRAST_MODE -> findViewById<View>(R.id.contrastFlag).visibility = visibility
            BRIGHTNESS_MODE -> findViewById<View>(R.id.brightnessFlag).visibility = visibility
            SATURATION_MODE -> findViewById<View>(R.id.saturationFlag).visibility = visibility
            EXPOSURE_MODE -> findViewById<View>(R.id.exposureFlag).visibility = visibility
            SHARPNESS_MODE -> findViewById<View>(R.id.sharpnessFlag).visibility= visibility
            BLUR_MODE -> findViewById<View>(R.id.blurFlag).visibility = visibility
        }
    }

    fun closeEditor(v: View){
        finish()
    }

    fun savePhoto(v: View){
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                WRITE_DATA_PRIMISSION_CODE
            )
        } else {
            val fullSizeBitmap =
                loadBitmap(this, photoUri)
            saveImageFile(processorManager.serialExecute(fullSizeBitmap!!, 0))
        }
    }

    private fun saveImageFile(finalBitmap:Bitmap) {
        val path = Environment.DIRECTORY_DCIM+"/Camera"
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT).format(Date())

        val file = File(Environment.getExternalStoragePublicDirectory(path), "IMG_$timeStamp.jpg")
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            finalBitmap.compress(CompressFormat.JPEG, 90, out)
            out.flush()
            out.close()
            galleryAddPic(this, Uri.fromFile(file))
            Toast.makeText(this, "Image saved", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == WRITE_DATA_PRIMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "write data permission granted", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "write data permission denied", Toast.LENGTH_LONG).show()
            }
        }
    }
}
