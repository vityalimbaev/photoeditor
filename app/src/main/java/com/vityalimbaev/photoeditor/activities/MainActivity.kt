package com.vityalimbaev.photoeditor.activities

import android.Manifest
import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.renderscript.RenderScript
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.vityalimbaev.photoeditor.R
import com.vityalimbaev.photoeditor.ScriptC_ImgProcessor
import com.vityalimbaev.photoeditor.getDisplayWidth
import com.vityalimbaev.photoeditor.loadScaledBitmap
import com.vityalimbaev.photoeditor.processors.BLUR_MODE
import com.vityalimbaev.photoeditor.processors.ProcessorsManger
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    private val CAMERA_REQUEST = 0
    private val GALLERY_REQUEST = 1
    private val CAMERA_PERMISSION_CODE = 100

    private var photoUri: Uri? = null
    private lateinit var photoPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createBlurBackground(findViewById(R.id.startImageView))
    }

    fun makePhoto(view: View) {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA),
                CAMERA_PERMISSION_CODE
            )
        } else {
            dispatchTakePictureIntent()
        }
    }

    fun choicePhoto(view: View) {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, GALLERY_REQUEST)
    }

    private fun startEditorActivity() {
        val intent = Intent(this, EditorActivity::class.java)
        intent.putExtra("photoUri", photoUri)
        startActivity(intent)
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            photoPath= absolutePath
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                photoFile?.also {
                     photoUri = FileProvider.getUriForFile(
                        this,
                        "com.vityalimbaev.photoeditor.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST)
                }
            }
        }
    }

    private fun createBlurBackground(imageView: ImageView) {
        val uri = Uri.Builder()
            .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
            .authority(resources.getResourcePackageName(R.drawable.background))
            .appendPath(resources.getResourceTypeName(R.drawable.background))
            .appendPath(resources.getResourceEntryName(R.drawable.background))
            .build()
        var bm = loadScaledBitmap(
            this,
            uri,
            getDisplayWidth(this)
        )
        val rs = RenderScript.create(this)
        var processor: ProcessorsManger
        for (i in 1..3) {
            processor = ProcessorsManger(bm, rs,
                ScriptC_ImgProcessor(rs)
            )
            bm = processor.executeProcess(BLUR_MODE, 100)
        }
        imageView.setImageBitmap(bm)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val intent = Intent(this, EditorActivity::class.java)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                GALLERY_REQUEST -> {
                    photoUri = data?.data!!
                }
                CAMERA_REQUEST -> {

                }
            }
            startEditorActivity()

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show()
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show()
            }
        }
    }
}
