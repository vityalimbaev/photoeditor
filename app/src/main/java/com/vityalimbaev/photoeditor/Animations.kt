package com.vityalimbaev.photoeditor

import android.content.Context
import android.transition.*
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.HorizontalScrollView
import androidx.constraintlayout.widget.ConstraintLayout


class Animations( context: Context) {
    fun open (seekBar: ConstraintLayout, scrollView: HorizontalScrollView ){
        if(scrollView.visibility == View.VISIBLE){
            scrollView.visibility = View.GONE
            seekBar.visibility = View.VISIBLE
        }else{
            scrollView.visibility = View.VISIBLE
            seekBar.visibility = View.GONE
        }
    }
}