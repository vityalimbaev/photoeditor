#pragma version(1)
#pragma rs java_package_name(com.vityalimbaev.photoeditor)

const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};
float width = 0.0f;
float hight = 0.0f;
float mValue = 0.0f;

static int histR[256] = {0}, histG[256] = {0}, histB[256] = {0};
static float scaleR;
static float scaleG;
static float scaleB;

rs_allocation inAllocation;
float3 maxValueLaplassian = (float3){1.0f,1.0f,1.0f};

static float3 getPixel(uint32_t x, uint32_t y){
    return rsUnpackColor8888(rsGetElementAt_uchar4(inAllocation,x, y)).rgb;
}

uchar4 RS_KERNEL saturationKernel(uchar4 in){
    float4 f4 = rsUnpackColor8888(in);
    float3 dotVector = dot(f4.rgb, gMonoMult);
    float3 newColor = mix(dotVector, f4.rgb, mValue);
    return rsPackColorTo8888(newColor);
}

uchar4 RS_KERNEL contrastKernel(uchar4 in){
    float4 f4 = rsUnpackColor8888(in);
    float3 rgb = f4.rgb;
    rgb = rgb - 0.5f;
    rgb = rgb * mValue;
    rgb = rgb + 0.5f;
    rgb = clamp(rgb, 0.0f, 1.0f);
    return rsPackColorTo8888(rgb);
}

uchar4 RS_KERNEL brightnessKernel(uchar4 in){
    float4 rgba = rsUnpackColor8888(in);
    float3 rgb = rgba.rgb;
    rgb = rgb + mValue;
    return rsPackColorTo8888(clamp(rgb,0.0f, 1.0f));
}

uchar4 RS_KERNEL exposureKernel(uchar4 in){
    float4 rgba = rsUnpackColor8888(in);
    float3 rgb = rgba.rgb;
    rgb = rgb * pow(2,mValue);
    return rsPackColorTo8888(clamp(rgb,0.0f, 1.0f));
}

uchar4 RS_KERNEL sharpnessKernel(uchar4 in, uint32_t x, uint32_t y){
    uchar4 out = in;
    if(x > 0 && x < width-1 && y > 0 && y < hight-1 ){
        float3 rgb = rsUnpackColor8888(in).rgb;
        float3 rgb_sharp =
            -getPixel(x-1, y-1) - getPixel(x,y-1) - getPixel(x+1,y-1) -
            getPixel(x+1,y) - getPixel(x+1,y+1) - getPixel(x,y+1) -
            getPixel(x-1, y+1) - getPixel(x-1,y)
            +(8.5f)*rgb;

        out = rsPackColorTo8888(clamp(rgb + mValue * rgb_sharp,0.0f, 1.0f));
    }
    return out;
}


void saturation(float value, rs_allocation in, rs_allocation out ){
    mValue = value;
    rsForEach(saturationKernel, in, out);
}

void contrast(float value, rs_allocation in, rs_allocation out){
    mValue = value;
    rsForEach(contrastKernel, in, out);
}

void brightness(float value, rs_allocation in, rs_allocation out){
    mValue = value;
    rsForEach(brightnessKernel, in, out);
}

void exposure(float value, rs_allocation in, rs_allocation out){
    mValue = value;
    rsForEach(exposureKernel,in, out);
}

void sharpness(float value, rs_allocation in, rs_allocation out){
    mValue = value;
    inAllocation = in;
    rsForEach(sharpnessKernel, in, out);
}
